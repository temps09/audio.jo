Le projet audio.jo a pour but de créer un nouveau format audio pour linux.
Ce projet est en trois étapes, la première est déja réalisée, la deuxième est en cours.
La première étape était de pouvoir créer des suites de variations de pressions produisant les sons avec quelques valeurs numériques.
La deuxième étape est de créer des générateurs de valeurs numériques pour avoir des banques audio de référence.
la troisième étape est de créer des fichiers audio sans limite de quantité.

Détail de la deuxième étape. Nous avons un générateur de fichier wav qui lit des fichiers composés de 200 ondes. Chaque onde possède 4 valeur, une pour le point bas, une pour le plateau de crète basse, une pour le point haut et une pour la crète haute. soit 800 valeurs numériques en fichier texte nommée forma audio.jo

J'ai commencé à présenter les voyelles, j'aurai besoin d'aide pour vérifier les codes, m'aider à trouver un classement, et identifier les sons.
Le générateur de fichier wav est conçu pour respecter les lois physiques de la nature, il dessine les ondes sous forme d'exponentielle amortie. Ce qui veut dire qu'au centre il y a peu de "matière" et au extrémité nous en avons beaucoup, ce qui crée des énergie cinétiques différentes. C'est en jouant avec ces suites d'énergie cinétiques en respectant les limites de plusieurs dimensions que nous créons les sons. 

La série app5 est orienté voix humaine

Le site internet qui présente des captures d'écran :
https://www.letime.net/son/

Et la banque de données des applications est ici :
https://www.letime.net/son/ap/