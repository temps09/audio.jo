# audio.jo



## Pourquoi

Le but de cette application est de rappeler le langage qui nous a permis d'évoluer pendant des siècles. Un langage qui se perd de plus en plus. Le langage est important car c'est l'outil qui nous permet de construire nos idées. Comme nous avons une similitude entre le langage et les lois de l'accoustique, et comme pour l'entendement les sons ont besoin d'être construit en respectant les lois, je présente une application qui prouve que l'algèbre et les fréquences ne sont que des incohérences issues d'un mauvais langage, un sylogisme du à l'utilisation d'un dictionnaire ou d'un moteur de recherche à la place d'une encyclopédie.

## Comment

Au travers d'une application je montre que comme pour l'entendement le même son suit la loi que trop de son masque le son, trop d'informations (inutiles ou fausses) masquent la forme qui nous fait entendre le son.

Le premier fichier proposé est un main.cpp, il se compile en terminal, lit un fichier nommé texte "audio.jo" contenant 200 ondes, et génère un fichier audio. Comme l'acoustique c'est plusieurs dimensions en équilibres, vous ne trouverez pas d'algèbre ou de fréquence ici, mais des variations d'inertie, qui produisent jusqu'aux voyelles ou consonnes. Il existe bien le cas particulier que certaines formes acoustiques ont besoin d'avoir le même nombre d'unité de temps pour avoir un son cristallin, mais c'est la cinétique qui crée le son, et en aucun cas ce cas particulier peut-être pris pour une généralité. Nous pouvons très bien masquer un marteau acoustique en le plaçant après un son fort, la résultante est toujours un équilibre entre plusieurs états entre les dimensions. Voir les démos dans la page des applications.

Les autres fichiers sont des applications qui génèrent des formes particulières dans le fichier audio.jo
Comme pour le main.cpp ces fichiers se compilent en terminal, mais je conseille fortement d'utiliser un tableur pour créer son fichier audio.jo

Le fichier audio.jo est un fichier texte contenant 200 ondes. Chaque onde est écrite à l'aide de 4 valeurs numériques. La première valeur est le point bas de l'onde, la deuxième valeur est le plateau du point bas, la troisième valeur est le point haut, et la quatrième valeur est le plateau d'unité de temps du haut.


## usage

Pour commencer la série des app5 (app5a, app5b ...) est des plus interessante, car elle met en évidence une des dimensions de l'acoustique, celle du changement d'inertie.
Le principe est de faire varier séparement la forme des ondes et d'en observer les effets. Cette démarche peut-être utilisé soit pour créer ses propres sons pour composer, soit par curiosité scientifique.
La série app5 est composée de deux vagues. La première vague contient 4 ondes et la deuxième vague 3 ondes. Avec 1 onde dans 2 ondes le son est plus cristalin, mais je préfère cette forme car la quantité devient une qualité, et permet d'observer plus facilement, l'oeil aime l'alignement qui est moins évident avec moins d'ondes (sous audacity). 

## page qui présente le début du projet 

Page qui présente le début du projet et une partie des lois physiques.
Les sons et les captures d'écrans essais de montrer.
C'est ici https://www.letime.net/son 
et la page qui présente les applications pour générer les différentes formes en fichier texte (audio.jo) est ici https://www.letime.net/son/ap/

## Mode composition musicale ./composeur

Pour composer de la musique, il faut en premier identifier les différentes formes utiles à la composition. Comme chaque forme est généré avec une application accompagné de ses arguments, il faut noter en exemple "joapp57 30 30 2" et ainsi de suite. Après, il faut compter le nombre de forme prévues. Il faut ouvrir un terminal , lancer ./composeur et indiquer le nombre de forme désiré. la première forme désirée est indiqué avec son indice en exemple 57 pour joapp57 puis entrer les valeurs 30 30 2, puis entrer la valeur 1, jusqu'à la demande de la deuxième forme, et ceci jusqu'à la dernière forme . Le fichier généré se nomme audiojo.wav, attention chaque nouvelle création efface l'ancienne, il faut renommer avant utilisation l'ancienne création si vous voulez la conserver.

## Mode lecteur musicale ./composox

Il faut en premier installer sox puis c'est comme pour la composition sauf qu'à la fin le fichier audio est joué

## Mode lecteur musicale ./josoxbass

Dans les suites évolutives apportées par sox cette version permet de modifier les bass et intègre un meilleur format audio, de l'ogg est stéréo

## Auteurs et remerciements

Il est difficile de se présenter comme auteur var je n'ai fait qu'appliquer le langage de la culture française qui m'a été enseignée. De plus de nombreux auteurs sur les forums du libre m'ont aidé à écrire les codes. En exemple composox avec l'intégration de libsox a été entièrement écrit par un autre, j'attends son autorisation pour donner son nom. Je remercie grandement tous ceux qui ont aidé la création de ce format audio, qui à mon sentiment l'outil le plus puissant pour pouvoir créer, car très léger et construit sur les lois des systèmes physiques.

## Licence 

GNU AFFERO GENERAL PUBLIC LICENSE

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://framagit.org/temps09/audio.jo.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://framagit.org/temps09/audio.jo/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
