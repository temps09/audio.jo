#include <iostream>
#include <fstream>
#include <string>
using namespace std;
/**pour compiler g++ 'app9.cpp'   -std=c++11 -Wall -Wextra -o 'joapp9' ********************/
int main()
{

    string const construit("audio.jo");
    ofstream ici(construit.c_str());

    int tab[801], tab1[4],j1,j2;
    
    for(int j=0; j<4; j++)
    {
    cout << "Tapez la variation des ondes, puis les 2 variations médianes, puis k7 " << j << " : ";
    cin >> tab1[j];
    }

    j1=1;
    j2 =2;
    for(int i=0; i<800; i++) {tab[i] = 128;}
    for(int i=1; i<800;i=i+2){ tab[i] = 2;}
    
  //  zone haute

    for(int i=2; i<23;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=26; i<47;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=50; i<71;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=74; i<96;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=98; i<120;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=122; i<144;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=146; i<168;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=170; i<192;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}
    
        
    for(int i=4; i<23;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=24; i<46;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=48; i<70;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=72; i<94;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=96; i<118;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=120; i<142;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=144; i<166;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=168; i<190;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}

  //  zone basse
    
    for(int i=194; i<216;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=218; i<240;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=242; i<264;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=266; i<288;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=290; i<312;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=314; i<336;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=338; i<360;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=362; i<384;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}

    
    for(int i=192; i<214;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=216; i<238;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=240; i<262;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=264; i<286;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=288; i<310;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=312; i<334;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=336; i<358;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=360; i<382;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    
  //  zone haute    
    
    for(int i=386; i<408;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}        
    for(int i=410; i<432;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=434; i<456;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=458; i<480;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=482; i<504;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=506; i<528;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=530; i<552;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=554; i<576;i=i+4) {  tab[i] = 128+(j1*(tab1[1]))+(j1*(tab1[0]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}
    
    for(int i=384; i<406;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=408; i<430;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=432; i<454;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=456; i<478;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=480; i<502;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=504; i<526;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=528; i<550;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=552; i<574;i=i+4) {  tab[i] = 128-(j2*(tab1[2]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}   
    
  //  zone basse    

    for(int i=578; i<600;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}        
    for(int i=602; i<624;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=626; i<648;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=650; i<672;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=674; i<696;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=698; i<720;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}    
    for(int i=722; i<744;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1++ ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=746; i<768;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=770; i<792;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}
    for(int i=794; i<800;i=i+4) {  tab[i] = 128+(j1*(tab1[1]));  j1-- ; if (tab[i]  > 250){tab[i] = 250;}}
    
   
    for(int i=576; i<598;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=600; i<622;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=624; i<646;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=648; i<670;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=672; i<694;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=696; i<718;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=720; i<742;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=744; i<766;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=768; i<790;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2++ ; if (tab[i] < 5){tab[i] = 5;}}
    for(int i=792; i<800;i=i+4) {  tab[i] = 128-(j2*(tab1[2]))-(j2*(tab1[0]));  j2-- ; if (tab[i] < 5){tab[i] = 5;}}
    
    tab[1] = tab1[3];
 
    if(ici)    
    {
      
        for(int i = 0; i < 800; i++) { ici<<tab[i]<< " "; }
       ici.close();
    }
    else
    {
        cout << "ERREUR: Impossible d'ouvrir le fichier." << endl;
    }
    return 0;
}
