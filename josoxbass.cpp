#include <cstdlib>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstring>
#include <sox.h>
#include <memory>
#include <string> 

/* pour compiler faire en terminal
sudo apt-get install libsox-dev
ldconfig -p | grep libsox
ldd /usr/lib/libsox.so
g++ josoxbass.cpp -std=c++11 -I/usr/local/include -L/usr/local/lib -lsox -Wall -Wextra -o josoxbass
*/
int main(void) 
{
    char appli[20], l[1];
    int j, a;
    std::cout << "Indiquez le nombre de formes à associer : "; std::cin >> j;
    std::cout << "Entrer une valeur entre 1 et 9 pour ajouter des bass :"; std::cin >> l;
    std::string joap = std::string("sox audiojo.wav jsoxbass.ogg vol -") + l + "dB bass +"+l;
     const int length = joap.length(); 

    char* char_array = new char[length + 1]; 

    strcpy(char_array, joap.c_str()); 
    

    for (int i = 0; i < j; i++) { 
        std::cout << "Entrez la reférence, exemple :57 pour joapp57 de la forme à associer : "; std::cin >> appli;
        for (int k = 0; k < 1; k++) {
            char joapp[20] = "./joapp";
            strcat(joapp, appli);
            std::system(joapp);
            std::cout << "Entrer la valeur 1, générateur fichier texte :"; std::cin >> a;
        }
        if (i == 0) {
            std::system("./executable");
        } else {
            std::system("./executable1");
        }
        std::cout << "Attendre 5s, puis tapez sur 1 pour poursuivre :"; std::cin >> a;
        std::system("./poids");
        std::cout << "Création de l'entete, attendre 2s puis tapez sur 1 pour poursuivre :"; std::cin >> a;
    }
    // Initialiser libsox
    sox_format_init();
    std::system(char_array);
    std::cout << "Entrer la valeur 1, modification des bass par sox :"; std::cin >> a;
    std::system("sox jsoxbass.ogg -c 2 josoxbass.ogg");
    std::cout << "Entrer la valeur 1, modifications par sox :"; std::cin >> a;
    std::system("play josoxbass.ogg");
    // Nettoyer libsox
    sox_format_quit();
}


